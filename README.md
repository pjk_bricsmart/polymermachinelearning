# PolymerMachineLearning

A repository for comments / code / data / results associated with machine learning applied to polymers.

## Literature

* Mannodi-Kanakkithodi, Arun, Ghanshyam Pilania, Tran Doan Huan, Turab Lookman, and Rampi Ramprasad.
"Machine learning strategy for accelerated design of polymer dielectrics." Scientific reports 6 (2016): 20952. 
(literature/ScientificReports_6_20952.pdf)  
* Huan, Tran Doan, Arun Mannodi-Kanakkithodi, Chiho Kim, Vinit Sharma, Ghanshyam Pilania, and Rampi Ramprasad.
"A polymer dataset for accelerated property prediction and design." Scientific data 3, no. 1 (2016): 1-10.
(literature/ScientificData_3_160012.pdf)  
* Wu, K., N. Sukumar, N. A. Lanzillo, C. Wang, Ramamurthy “Rampi” Ramprasad, R. Ma, A. F. Baldwin, G. Sotzing, and C. Breneman.
"Prediction of polymer properties using infinite chain descriptors (ICD) and machine learning: Toward optimized dielectric polymeric materials."
Journal of Polymer Science Part B: Polymer Physics 54, no. 20 (2016): 2082-2091.
(literature/Wu_et_al-2016-Journal_of_Polymer_Science_Part_B__Polymer_Physics.pdf)  
* Mannodi-Kanakkithodi, Arun, Ghanshyam Pilania, and Rampi Ramprasad.
"Critical assessment of regression-based machine learning methods for polymer dielectrics."
Computational Materials Science 125 (2016): 123-135.
(literature/CompMatlSci_125_123.pdf)  
* Mannodi-Kanakkithodi, Arun, Anand Chandrasekaran, Chiho Kim, Tran Doan Huan, Ghanshyam Pilania, Venkatesh Botu, and Rampi Ramprasad.
"Scoping the polymer genome: A roadmap for rational polymer dielectrics design and beyond."
Materials Today 21, no. 7 (2018): 785-796.
(literature/MaterialsToday_21_7)  
* Audus, Debra J., and Juan J. de Pablo.
"Polymer informatics: opportunities and challenges." (2017): 1078-1082.
(literature/ACSMacroLett_6_1078.pdf)  
* Kim, Chiho, Anand Chandrasekaran, Tran Doan Huan, Deya Das, and Rampi Ramprasad.
"Polymer genome: a data-powered polymer informatics platform for property predictions."
The Journal of Physical Chemistry C 122, no. 31 (2018): 17575-17585.
(literature/JPhysChemC_122_17575.pdf)  
* Kumar, Jatin N., Qianxiao Li, and Ye Jun.
"Challenges and opportunities of polymer design with machine learning and high throughput experimentation."
MRS Communications 9, no. 2 (2019): 537-544.
(literature/Kumar et al. - 2019 - Challenges and opportunities of polymer design wit.pdf)  
* Wu, Stephen, Yukiko Kondo, Masa-aki Kakimoto, Bin Yang, Hironao Yamada, Isao Kuwajima, Guillaume Lambard et al.
"Machine-learning-assisted discovery of polymers with high thermal conductivity using a molecular design algorithm."
npj Computational Materials 5, no. 1 (2019): 1-11.
(literature/Wu and al. - Machine-learning-assisted discovery.pdf)  